"use client"; // this is a client component 👈🏽
import React from 'react'
import { ThemeProvider } from 'next-themes'

export default function Providers({children}) {
  return (
    <ThemeProvider disableTransitionOnChange>
        {children}
    </ThemeProvider>
  )
}
