import './globals.css'
import Nav from './nav'
import Providers from './Providers'
import { Montserrat } from '@next/font/google'


const montserrat = Montserrat({
  weight: ['400', '700'],
  subsets: ['latin'],
})

export default function RootLayout({ children }) {
  return (
    
    <html lang="en">
      <head />
        <body>
          <div className={montserrat.className}>
          <Providers>
            <Nav/>
            {children}
          </Providers>
          </div>
        </body>
    </html>
  )
}
