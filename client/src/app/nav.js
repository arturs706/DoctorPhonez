"use client"; // this is a client component 👈🏽

import React from 'react'
import styles from './nav.module.css'
import Hamburger from './hamburger'
import Image from 'next/image'
import { useTheme } from 'next-themes'
import { useEffect, useState } from 'react'
import { DarkModeSwitch } from 'react-toggle-dark-mode';
import Link from 'next/link'
import Navpage from './navpage';


export default function Nav(){
  const [mounted, setMounted] = useState(false)
  const { theme, setTheme } = useTheme()
  const [isDarkMode, setDarkMode] = useState(false);
  const [isOpen, setIsOpen] = useState(false);
  const [scrollPosition, setScrollPosition] = useState(0);
  //read the Y axis of the scroll position and set the state
  const handleScroll = () => {
    const position = window.pageYOffset;
    setScrollPosition(position);
  };
  //add the event listener when the component mounts
  useEffect(() => {
    window.addEventListener('scroll', handleScroll, { passive: true });
    //remove the event listener when the component unmounts
    return () => {
      window.removeEventListener('scroll', handleScroll);
    };
  }, []);
  //define the inline styles for the nav
  const navStyle = {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    textAlign: 'center',
    width: '100%',
    zIndex: '99'
  };
  
  //if the scroll position is greater than 283, add the sticky class to the nav
  if (scrollPosition < 280) {
    navStyle.position = 'fixed';
  }
    

  
  
  const handleToggle = () => {
    setIsOpen(!isOpen);
  }
  

  const toggleDarkMode = () => {
    setDarkMode(!isDarkMode);
    setTheme(isDarkMode ? 'light' : 'dark');
  };
  useEffect(() => {
    if (theme === 'dark') {
      setDarkMode(true)
    } else {
      setDarkMode(false)
    }
  }, [theme])
    useEffect(() => {
      setMounted(true)
    }, [])
  
    if (!mounted) {
      return null
    }

  return (
    <div style={navStyle}>
        <div className={styles.burger}>
          <Hamburger isOpen={isOpen} handleToggle={handleToggle} />
          {isOpen && <Navpage />}
        </div>

        <div className={styles.list}>
            <Link href="/"><span>Home</span></Link>
            <Link href="/phones"><span>Mobile Phones</span></Link>
            <Link href="/tablets"><span>Tablets</span></Link>
            <Link href="/accessories"><span>Accessories</span></Link>
            <Link href="/deals"><span>Deals</span></Link>
            <Link href="/contactus"><span>Contact us</span></Link>

        </div>
        <div className={styles.rightlist}>
            <DarkModeSwitch
              checked={isDarkMode}
              onChange={toggleDarkMode}
              size={30}
              sunColor="#ebebeb"
              style={{opacity: "0.8", marginBottom: "4px", strokeWidth: "1px"}}
            />
            {
              theme === 'dark' ? (
                <>
                  <Image src="/searchwhite.svg" alt="search" width={20} height={20} />
                  <Link href="/cart"><Image src="/bagwhite.svg" alt="search" width={20} height={20} /></Link>
                </>
                ) : (
                <>
                  <Image src="/search.svg" alt="search" width={20} height={20} />
                  <Link href="/cart"><Image src="/bag.svg" alt="search" width={20} height={20} /></Link>
                </>

              )        
            }
            <div>
              <Link href="/login" className={styles.signin}>Sign In</Link>
            </div>
        </div>
    </div>
  )
}
