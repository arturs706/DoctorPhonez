"use client"; // this is a client component 👈🏽

import { useEffect, useRef } from "react";
import styles from "./page.module.css";
import Image from "next/image";
import {ScrollTrigger} from 'gsap/dist/ScrollTrigger';
import gsap from 'gsap';


export default function Page() {
  gsap.registerPlugin(ScrollTrigger);
  const mainImage = useRef(null);
  useEffect(() => {
    const tl = gsap.timeline({
      scrollTrigger: {
        trigger: mainImage.current,
        start: "100 240",
        end: "+=280",
        scrub: true,
        pin: true,
        pinSpacing: true
      }
    });
    tl.fromTo (
      mainImage.current,
      { scale: 1 },
      { scale: 1.4, duration: 1}
    );

    return () => {
      tl.revert();
    };
  }, []);





  return (
    <>
      <main>
        <div className={styles.firstdiv}>
        <Image
          className={styles.mainimage}
          src="https://res.cloudinary.com/dttaprmbu/image/upload/v1676932495/random/uk-galaxy-s23-s916-sm-s916bzkgeub-535048319_vl39he.avif" 
          alt="logo"
          height={700}
          width={800}
          quality={100}
          ref={mainImage}
          priority={true}
        />
        </div>
        <div className={styles.firstdiv}></div>

      </main>
    </>
  );
}
