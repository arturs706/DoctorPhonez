use actix_web::{get, App, HttpResponse, HttpServer, Responder};
use actix_cors::Cors;



#[get("/api/v1")]
async fn hello() -> impl Responder {
    HttpResponse::Ok().json("Hello world!")
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    HttpServer::new(move || {
        let cors = Cors::default().allow_any_origin().allow_any_method().allow_any_header();
        App::new()
            .wrap(cors)
            .service(hello)
    })
    .bind("0.0.0.0:10000")?
    .run()
    .await
}

